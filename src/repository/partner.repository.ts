import { Injectable, Logger } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { Partner } from 'src/entities/partner.entity';
import { IRegisterPartner } from 'src/interface/register-partner.interface';

@Injectable()
export class PartnerRepository extends Repository<Partner> {
  private readonly logger = new Logger(PartnerRepository.name);
  constructor(dataSource: DataSource) {
    super(Partner, dataSource.createEntityManager());
  }

  async savePartner(partner: IRegisterPartner): Promise<Partner> {
    const newPartner = await this.save(partner);
    this.logger.log('Insert new partner');
    return newPartner;
  }
}
