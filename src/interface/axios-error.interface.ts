export interface IAxiosError {
  response: IAxiosBaseError;
}

export interface IAxiosBaseError {
  statusCode: number;
  message: string;
  error?: string;
}
