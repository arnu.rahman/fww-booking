export interface IReservationCheck {
  passengerIdentity: string;
  flightDate: string;
}
