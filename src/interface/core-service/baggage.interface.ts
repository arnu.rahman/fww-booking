export interface IBaggage {
  id?: string;
  capacity: number;
  category: string;
  chargePrice: number;
}
