import { IAirplane } from '../airplane.interface';

export interface IFlightSeatResponse {
  id: string;
  code: string;
  origin: string;
  destination: string;
  airplane: IAirplane;
  departureTime: string;
  arrivalTime: string;
  durationInMinutes?: number;
}
