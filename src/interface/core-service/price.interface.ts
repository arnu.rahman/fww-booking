import { ESeatClass } from 'src/helpers/enum/seat-class.enum';

export interface IPrice {
  id?: string;
  flight?: string;
  seatClass: ESeatClass;
  price: number;
  seatLeft?: number;
}
