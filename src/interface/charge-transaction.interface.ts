import { EBankChoice } from 'src/helpers/enum/bank-choice.enum';

export interface IChargeTransaction {
  bank: EBankChoice;
  virtualAccountNumber?: string;
  billerCode?: string;
  billerKey?: string;
}
