import { IsEnum, IsNotEmpty, IsUUID, IsString } from 'class-validator';
import { EBankChoice } from 'src/helpers/enum/bank-choice.enum';

export class ChargePaymentDto {
  @IsEnum(EBankChoice)
  @IsNotEmpty()
  bank: EBankChoice;

  @IsUUID()
  @IsNotEmpty()
  reservationId: string;

  @IsString()
  @IsNotEmpty()
  bookingCode: string;
}
