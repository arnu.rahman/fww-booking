export interface IFlightCandidateRequest {
  originAirportId: string;
  destinationAirportId: string;
}
