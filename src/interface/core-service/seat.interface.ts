import { ESeatPosition } from 'src/helpers/enum/seat-position.enum';
import { ESeatClass } from 'src/helpers/enum/seat-class.enum';
import { ESeatSide } from 'src/helpers/enum/seat-side.enum';

export interface ISeat {
  id: string;
  code: string;
  seatClass: ESeatClass;
  seatSide: ESeatSide;
  seatPosition: ESeatPosition;
  airplane?: string;
  seqRow: number
}
