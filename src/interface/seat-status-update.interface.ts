import { ESeatStatus } from 'src/helpers/enum/seat-status.enum';

export interface ISeatStatusUpdate {
  flightDate: string;
  flightId: string;
  seatId: string;
  seatStatus: ESeatStatus;
}
