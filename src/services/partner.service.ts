import { Injectable, Logger } from '@nestjs/common';
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserPool,
  CognitoUserAttribute
} from 'amazon-cognito-identity-js';
import { PartnerRepository } from 'src/repository/partner.repository';
import { AuthConfig } from 'src/config/auth.config';
import { LoginRequestDto } from 'src/dto/login.request.dto';
import { ITokenLogin } from 'src/interface/token-login.interface';
import { RegisterRequestDto } from 'src/dto/register-patner.dto';
import { IRegisterPartner } from 'src/interface/register-partner.interface';

@Injectable()
export class PartnerService {
  private readonly logger = new Logger(PartnerService.name);
  private readonly userPool: CognitoUserPool;

  constructor(
    private authConfig: AuthConfig,
    private readonly partnerRepo: PartnerRepository,
  ) {
    this.userPool = new CognitoUserPool({
      UserPoolId: this.authConfig.userPoolId,
      ClientId: this.authConfig.clientId,
    });
  }

  async register(registerDto: RegisterRequestDto): Promise<IRegisterPartner> {
    const { username, name, email, phone, password } = registerDto;

    const newUser = new Promise<IRegisterPartner>((resolve, reject) => {
      return this.userPool.signUp(
        username,
        password,
        [
          new CognitoUserAttribute({ Name: 'email', Value: email }),
          new CognitoUserAttribute({
            Name: 'phone_number',
            Value: phone,
          }),
        ],
        null,
        (error, result) => {
          if (!result) {
            this.logger.log(`Register new member to cognito failed because ${error} `);
            reject(error)
          } else {
            const user: IRegisterPartner = {
              cognitoId: result.userSub,
              username,
              name,
              email,
              phone,
            };
            this.logger.log('Register new member to cognito');
            resolve(user);
          }
        },
      );
    });

    const dataPartner = await newUser;
    const newPartner = await this.partnerRepo.savePartner(dataPartner);
    this.logger.log('Process data new partner');
    return newPartner;
  }

  async login(user: LoginRequestDto): Promise<ITokenLogin> {
    const { username, password } = user;
    const authenticationDetails = new AuthenticationDetails({
      Username: username,
      Password: password,
    });

    const userData = {
      Username: username,
      Pool: this.userPool,
    };
    const cognitoUser = new CognitoUser(userData);

    const token = new Promise<ITokenLogin>((resolve, reject) => {
      return cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
          const accessToken = result.getIdToken().getJwtToken();
          const refreshToken = result.getRefreshToken().getToken();
          this.logger.log('Login cognito successfully');
          resolve({ accessToken, refreshToken } as ITokenLogin);
        },
        onFailure: (error) => {
          this.logger.log(`Login cognito failed because ${error}`);
          reject(error)
        },
      });
    });

    this.logger.log('Login successfully');
    return await token;
  }
}
