import { Module } from '@nestjs/common';
import { InquiryController } from './controller/inquiry.controller';
import { InquiryService } from './services/inquiry.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';
import { SeatStatusRepository } from './repository/seat-status.repository';
import { CoreService } from './services/core.service';
import { BPMService } from './services/bpm.service';
import { WorkflowRepository } from './repository/workflow.repository';
import { WorkflowDetailRepository } from './repository/workflow-detail.repository';
import { WorkflowController } from './controller/workflow.controller';
import { WorkflowService } from './services/workflow.service';
import { PaymentService } from './services/payment.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AuthModule } from './auth/auth.module';
import { ReservationController } from './controller/reservation.controller';
import { ReservationService } from './services/reservation.service';
import { PartnerController } from './controller/partner.controller';
import { PartnerService } from './services/partner.service';
import { PartnerRepository } from './repository/partner.repository';
import { dataSourceOptions } from './config/datasource.config';
import { HelperService } from './services/helper.service';
import { MemberService } from './services/member.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    LoggerModule.forRoot({
      pinoHttp: {
        formatters: {
          level: (label: string) => {
            return { level: label.toUpperCase() };
          },
        },
        customLevels: {
          emergerncy: 80,
          alert: 70,
          critical: 60,
          error: 50,
          warn: 40,
          notice: 30,
          info: 20,
          debug: 10,
        },
        useOnlyCustomLevels: true,
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
            colorize: true,
            levelFirst: true,
            translateTime: 'SYS:standard',
            ignore: 'hostname,pid',
          },
        },
      },
    }),
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
        maxRedirects: 5,
      }),
    }),
    TypeOrmModule.forRoot(dataSourceOptions),

    ClientsModule.registerAsync([
      {
        name: 'CoreEngine',
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [configService.get<string>('RABBIT_MQ_URI')],
            queue: configService.get<string>('RABBIT_MQ_CORE_QUEUE'),
            queueOptions: { durable: false },
            prefetchCount: 1,
          },
        }),
      },
    ]),
    AuthModule,
  ],
  controllers: [
    InquiryController,
    ReservationController,
    WorkflowController,
    PartnerController,
  ],
  providers: [
    InquiryService,
    ReservationService,
    HelperService,
    CoreService,
    MemberService,
    BPMService,
    PaymentService,
    WorkflowService,
    PartnerService,
    SeatStatusRepository,
    WorkflowRepository,
    WorkflowDetailRepository,
    PartnerRepository,
  ],
})
export class AppModule { }
