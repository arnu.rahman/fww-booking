import { Injectable, Logger } from '@nestjs/common';
import { WorkflowDetail } from 'src/entities/workflow-detail.entity';
import { IWorkflowDetail } from 'src/interface/workflow.interface';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class WorkflowDetailRepository extends Repository<WorkflowDetail> {
  private readonly logger = new Logger(WorkflowDetailRepository.name);
  constructor(dataSource: DataSource) {
    super(WorkflowDetail, dataSource.createEntityManager());
  }

  async saveWorkflowDetail(
    dataWorkflow: IWorkflowDetail,
  ): Promise<WorkflowDetail> {
    const newWorkflow = await this.save(dataWorkflow);
    this.logger.log('Insert new workflow detail');
    return newWorkflow;
  }
}
