import {
  BadRequestException,
  Injectable,
  Logger,
  UnprocessableEntityException,
} from '@nestjs/common';
import { SeatStatusRepository } from 'src/repository/seat-status.repository';
import { CoreService } from './core.service';
import { ReservationDto } from 'src/dto/reservation.dto';
import { IReservationCheck } from 'src/interface/reservation-check.interface';
import { ESeatStatus } from 'src/helpers/enum/seat-status.enum';
import { IReservationNew } from 'src/interface/reservation-new.interface';
import { BPMService } from './bpm.service';
import { IStartProcess } from 'src/interface/bpm-service/start-process.interface';
import { WorkflowRepository } from 'src/repository/workflow.repository';
import { IWorkflowMaster } from 'src/interface/workflow.interface';
import { CognitoUserDto } from 'src/dto/cognito-user.dto';
import { HelperService } from './helper.service';
import * as moment from 'moment';
import { MemberService } from './member.service';

@Injectable()
export class ReservationService {
  private readonly logger = new Logger(ReservationService.name);

  constructor(
    private readonly coreService: CoreService,
    private readonly memberService: MemberService,
    private readonly bpmService: BPMService,
    private readonly helperService: HelperService,
    private readonly seatStatusRepo: SeatStatusRepository,
    private readonly workflowRepo: WorkflowRepository,
  ) {}

  async makeReservation(user: CognitoUserDto, reservationDto: ReservationDto) {
    const { timezone, ...dataReservation } = reservationDto;
    const todayString = this.helperService.formatingTimeWithTimezone(timezone);

    //validate if flight date before today
    if (moment(reservationDto.flightDate).isBefore(todayString)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${todayString}`,
      );
    }

    const reservationCheck: IReservationCheck = {
      passengerIdentity: reservationDto.identityNumber,
      flightDate: reservationDto.flightDate,
    };

    const resultCheckSeatAndFlight = await Promise.all([
      this.seatStatusRepo.findOneByPassengerAndFlightDate(reservationCheck),
      this.coreService.fetchFlightAirport(reservationDto.flightId),
    ]);

    //check member
    let memberData
    if(reservationDto.memberId){
      memberData = this.memberService.fetchMember(reservationDto.memberId)
    }

    const seatStatus = resultCheckSeatAndFlight[0];
    const flightAirport = resultCheckSeatAndFlight[1];

    if (seatStatus.flightId !== reservationDto.flightId) {
      throw new UnprocessableEntityException(
        `Failed to make reservation due to have another flight : ${seatStatus.flightId}`,
      );
    }

    if (seatStatus.seatId !== reservationDto.seatId) {
      throw new UnprocessableEntityException(
        `Failed to make reservation due to seat misplaced : ${seatStatus.seatId}`,
      );
    }

    if (seatStatus.status !== ESeatStatus.SELECTED) {
      throw new UnprocessableEntityException(
        `Failed to make reservation due to seat status ${seatStatus.seatId} is not selected`,
      );
    }

    const reservationTime = new Date().toISOString();

    const dataReservationNew: IReservationNew = {
      partnerId: user.cognitoId,
      ...dataReservation,
      reservationTime,
      priceActual: seatStatus.price,
    };

    const reservation = await this.coreService.submitReservation(
      dataReservationNew,
    );
    if (!reservation) {
      throw new UnprocessableEntityException(`Failed to make reservation`);
    }

    const dataWorkflow: IStartProcess = {
      identityNumber: reservationDto.identityNumber,
      name: reservationDto.name,
      birthDate: reservationDto.birthDate,
      isMember: memberData ? 'Yes' : 'No',
      memberId: reservationDto.memberId ? reservationDto.memberId : null,
      email: reservationDto.email,
      phone: reservationDto.phone,
      airplane: flightAirport.airplane.name,
      flightId: flightAirport.id,
      flightCode: flightAirport.code,
      flightDate: reservationDto.flightDate,
      originAirportId: flightAirport.origin.id,
      originAirportCode: flightAirport.origin.code,
      originAirportName: flightAirport.origin.name,
      originAirportCity: flightAirport.origin.city,
      departureTime: flightAirport.departureTime,
      departureTimezone: flightAirport.origin.timezone,
      destinationAirportId: flightAirport.destination.id,
      destinationAirportCode: flightAirport.destination.code,
      destinationAirportName: flightAirport.destination.name,
      destinationAirportCity: flightAirport.destination.city,
      arrivalTime: flightAirport.arrivalTime,
      arrivalTimezone: flightAirport.destination.timezone,
      boardingTime: flightAirport.boardingTime,
      seatId: seatStatus.seatId,
      seatNumber: seatStatus.seatNumber,
      reservationTime,
      timezone,
      bussinessKey: reservation.id,
    };

    seatStatus.status = ESeatStatus.BOOKED;

    const result = await Promise.all([
      this.bpmService.startProcess(dataWorkflow),
      this.seatStatusRepo.save(seatStatus),
    ]);

    const newProcess = result[0];
    const workflowNew: IWorkflowMaster = {
      processInstanceId: newProcess.id,
      reservationId: reservation.id,
    };
    await this.workflowRepo.saveWorkflow(workflowNew);

    this.logger.log('Process reservation to workflow engine, core service');
    return {
      messageResponse:
        'Success make a reservation, we will deliver booking code via email as soon as possible',
      data: {
        id: reservation.id,
        reservationTime:
          this.helperService.reformattingTimeFollowTimezone(
            reservationTime,
            timezone,
          ),
        status: reservation.status,
      },
    };
  }
}
