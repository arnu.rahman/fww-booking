import { HttpService } from '@nestjs/axios';
import {
  Injectable,
  Logger,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, lastValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import {
  IAxiosBaseError,
  IAxiosError,
} from 'src/interface/axios-error.interface';
import { IChargeRequest } from 'src/interface/payment-service/charge-request.interface';
import { IStatusPaymentResponse } from 'src/interface/payment-service/status-payment-response.interface';
import { IChargeResponse } from 'src/interface/payment-service/charge-response.interface';

@Injectable()
export class PaymentService {
  private readonly logger = new Logger(PaymentService.name);

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
  ) {}

  private readonly baseUrl = `${this.configService.get<string>('PGMID_URL')}`;

  async createCharge(dataCharge: IChargeRequest) {
    const url = `${this.baseUrl}/charge`;
    this.logger.log(`Charge midtrans: ${url}`);
    const headersRequest = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Basic ${this.configService.get<string>('PGMID_BASIC')}`,
    };

    const config = {
      headers: headersRequest,
      timeout: 30000,
    };

    const { data } = await lastValueFrom(
      this.httpService.post(url, dataCharge, config).pipe(
        catchError((error: AxiosError) => {
          console.log(error)
          const axiosBaseError = error.config.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Create charge midtrans');
    try {
      const expectedResult: IChargeResponse =
        data as IChargeResponse;
      this.logger.log('Success request charge to midtrans');
      return expectedResult;
    } catch (error) {
      throw new UnprocessableEntityException(
        'Unexpected response from midtrans',
      );
    }
  }

  async viewStatus(id: string) {
    const url = `${this.baseUrl}/${id}/status`;

    const headersRequest = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Basic ${this.configService.get<string>('PGMID_BASIC')}`,
    };

    const config = {
      headers: headersRequest,
      timeout: 30000,
    };

    const { data } = await lastValueFrom(
      this.httpService.get(url, config).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.config.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    try {
      const expectedResult: IStatusPaymentResponse =
        data as unknown as IStatusPaymentResponse;
      this.logger.log('Get status payment from midtrans');
      return expectedResult;
    } catch (error) {
      throw new UnprocessableEntityException(
        'Unexpected response from midtrans',
      );
    }
  }
}
