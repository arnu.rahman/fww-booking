import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { AuthConfig } from '../config/auth.config';
import { CognitoStrategy } from './cognito.strategy';
import { BasicStrategy } from './authbasic.strategy';

@Module({
  imports: [PassportModule, ConfigModule],
  providers: [BasicStrategy, AuthConfig, CognitoStrategy],
  exports: [AuthConfig],
})
export class AuthModule {}
