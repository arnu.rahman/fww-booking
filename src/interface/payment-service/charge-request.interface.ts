export interface IChargeRequest {
  payment_type: string;
  bank_transfer?: object;
  echannel?: object;
  transaction_details: ITransactionDetail;
  item_details: IItemDetail[];
  customer_details: ICustomerDetail;
}

export interface ITransactionDetail {
  order_id: string;
  gross_amount: number;
}

export interface IItemDetail {
  id: string;
  name: string;
  quantity: number;
  price: number;
}

export interface ICustomerDetail {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
}
