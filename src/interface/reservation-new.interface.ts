import { EIdentityCat } from "src/helpers/enum/identity-category.enum";

export interface IReservationNew {
  partnerId: string;
  memberId?: string;
  identityCategory: EIdentityCat;
  identityNumber: string;
  name: string;
  birthDate: string;
  phone: string;
  email: string;
  flightDate: string;
  flightId: string;
  seatId: string;
  priceActual: number;
  reservationTime: string;
}
