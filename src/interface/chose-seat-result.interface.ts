import { ESeatStatus } from 'src/helpers/enum/seat-status.enum';

export interface IChoseSeatResult {
  passenger: string;
  flightDate: string;
  flightId: string;
  seatId: string;
  status: ESeatStatus;
}
