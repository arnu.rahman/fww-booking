import { Injectable, Logger } from '@nestjs/common';
import { Workflow } from 'src/entities/workflow.entity';
import { IWorkflowMaster } from 'src/interface/workflow.interface';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class WorkflowRepository extends Repository<Workflow> {
  private readonly logger = new Logger(WorkflowRepository.name);
  constructor(dataSource: DataSource) {
    super(Workflow, dataSource.createEntityManager());
  }

  async saveWorkflow(dataWorkflow: IWorkflowMaster): Promise<Workflow> {
    const newWorkflow = await this.save(dataWorkflow);
    this.logger.log('Insert new workflow');
    return newWorkflow;
  }

  async getWorkflowByReservationId(reservationId: string): Promise<Workflow> {
    const workflow = await this.findOne({
      where: { reservationId },
    });
    this.logger.log('Query data workflow');
    return workflow;
  }
}
