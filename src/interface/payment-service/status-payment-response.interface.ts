import { IVANumber } from './charge-response.interface';

export interface IStatusPaymentResponse {
  status_code: string;
  transaction_id: string;
  gross_amount: string;
  currency: string;
  order_id: string;
  payment_type: string;
  signature_key: string;
  transaction_status: string;
  fraud_status: string;
  status_message: string;
  merchant_id: string;
  va_numbers?: IVANumber[];
  payment_amount?: object[];
  transaction_time: string;
  settlement_time?: string;
  expiry_time: string;
}
