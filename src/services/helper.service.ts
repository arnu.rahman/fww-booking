import { Injectable, Logger } from '@nestjs/common';
import { EBankChoice } from 'src/helpers/enum/bank-choice.enum';
import { ESeatClass } from 'src/helpers/enum/seat-class.enum';
import { ETimezoneCode } from 'src/helpers/enum/timezone-code.enum';
import { ETimezone } from 'src/helpers/enum/timezone.enum';
import { IStartProcess } from 'src/interface/bpm-service/start-process.interface';
import { IChargeTransaction } from 'src/interface/charge-transaction.interface';
import { IFlight } from 'src/interface/core-service/flight.interface';
import { IPrice } from 'src/interface/core-service/price.interface';
import { IFlightBaggageResponse } from 'src/interface/core-service/response/flight-baggage-response.interface';
import { IFlightCandidateResponse } from 'src/interface/core-service/response/flight-candidate-response.interface';
import { IFlightSeatResponse } from 'src/interface/core-service/response/flight-seat-response.interface';
import { IOccupiedFlightCandidate } from 'src/interface/occupied-flight-candidate.interface';
import { IChargeResponse } from 'src/interface/payment-service/charge-response.interface';
import * as moment from 'moment';

@Injectable()
export class HelperService {
  private readonly logger = new Logger(HelperService.name);

  private __addNHours(timeString: string, add: number): string {
    const timeStringArrays = timeString.split(':');
    const hour = Number(timeStringArrays[0]);
    let newHour = hour + add;
    if (newHour >= 24) {
      newHour = newHour - 24;
    }
    const newHourString = String(newHour).padStart(2, '0');
    timeStringArrays[0] = newHourString;
    return timeStringArrays.join(':');
  }

  formattingFlightCandidate(
    flights: IFlightCandidateResponse[],
    occupations: IOccupiedFlightCandidate[],
    flightDate: string,
  ) {

    const newFlights = flights.map(
      ({
        airplane,
        prices,
        ...flight
      }) => {
        const { id, ...newAirplane } = airplane;

        const newPrices = prices.map(({ id, ...price }) => {
          let remains =
            price.seatClass === ESeatClass.BUSINESS
              ? airplane.maxBusiness
              : airplane.maxEconomy;

          let flightOccupation: IOccupiedFlightCandidate;

          if (price.seatClass === ESeatClass.BUSINESS) {
            flightOccupation = occupations.find(
              (occ) =>
                occ.flight === flight.id &&
                occ.seatClass === ESeatClass.BUSINESS,
            );
          }

          if (price.seatClass === ESeatClass.ECONOMY) {
            flightOccupation = occupations.find(
              (occ) =>
                occ.flight === flight.id &&
                occ.seatClass === ESeatClass.ECONOMY,
            );
          }

          remains = flightOccupation
            ? remains - Number(flightOccupation.occupied)
            : remains;

          return {
            ...price,
            seatLeft: remains,
          } as IPrice;
        });

        this.logger.log('Reformat data flight candidates');

        return {
          ...flight,
          flightDate,
          airplane: newAirplane,
          prices: newPrices
        } as IFlight;
      },
    );
    return newFlights;
  }

  formattingInfoBaggage(flight: IFlightBaggageResponse) {
    const { id, code, airplane, baggage } = flight;
    const newBaggages = baggage.map(({ id, ...baggage }) => baggage);
    const newFlight: IFlight = {
      id,
      code,
      airplane,
      baggage: newBaggages,
    };
    return newFlight;
  }

  formattingAvailableSeat(
    flightSeats: IFlightSeatResponse,
    occupied: string[],
  ) {
    const {
      airplane: { seats },
    } = flightSeats;
    const seatConfigs = seats;
    const seatAvailables = seatConfigs.filter(
      (seat) => !occupied.includes(seat.id),
    );

    const seatWithoutAirPlane = seatAvailables.map(
      ({ airplane, ...seat }) => seat,
    );

    const { id, ...newAirplane } = flightSeats.airplane;
    newAirplane.seats = seatWithoutAirPlane;

    const newFlight: IFlight = {
      id: flightSeats.id,
      code: flightSeats.code,
      airplane: newAirplane,
    };

    return newFlight;
  }

  formatingTimeWithTimezone(timezone: ETimezone): string {
    let utcTime: number;
    if (timezone === ETimezone.WIB) {
      utcTime = 0
    } else if (timezone === ETimezone.WITA) {
      utcTime = 1
    } else {
      utcTime = 2
    }

    return moment().add(utcTime, 'hours').format('YYYY-MM-DD')
  }

  reformattingTimeFollowTimezone(
    utcString: string,
    timezone: ETimezone,
  ): string {
    let utcTime: number;
    if (timezone === ETimezone.WIB) {
      utcTime = 0
    } else if (timezone === ETimezone.WITA) {
      utcTime = 1
    } else {
      utcTime = 2
    }

    return moment(utcString).add(utcTime, 'hours').format('YYYY-MM-DD')
  }

  formattingStartProcessWorkflow(dataWorkflow: IStartProcess) {
    return {
      variables: {
        identityNumber: {
          value: dataWorkflow.identityNumber,
          type: 'String',
        },
        name: {
          value: dataWorkflow.name,
          type: 'String',
        },
        birthDate: {
          value: dataWorkflow.birthDate,
          type: 'String',
        },
        isMember: {
          value: dataWorkflow.isMember,
          type: 'String',
        },
        memberId: {
          value: dataWorkflow.memberId,
          type: 'String',
        },
        email: {
          value: dataWorkflow.email,
          type: 'String',
        },
        phone: {
          value: dataWorkflow.phone,
          type: 'String',
        },
        flightCode: {
          value: dataWorkflow.flightCode,
          type: 'String',
        },
        airplane: {
          value: dataWorkflow.airplane,
          type: 'String',
        },
        flightDate: {
          value: dataWorkflow.flightDate,
          type: 'String',
        },
        departureAirport: {
          value: dataWorkflow.originAirportName,
          type: 'String',
        },
        departureCode: {
          value: dataWorkflow.originAirportCode,
          type: 'String',
        },
        departureCity: {
          value: dataWorkflow.originAirportCity,
          type: 'String',
        },
        departureTime: {
          value: dataWorkflow.departureTime,
          type: 'String',
        },
        departureTimezone: {
          value: dataWorkflow.departureTimezone,
          type: 'String',
        },
        destinationAirport: {
          value: dataWorkflow.destinationAirportName,
          type: 'String',
        },
        destinationCode: {
          value: dataWorkflow.destinationAirportCode,
          type: 'String',
        },
        destinationCity: {
          value: dataWorkflow.destinationAirportCity,
          type: 'String',
        },
        arrivalTime: {
          value: dataWorkflow.arrivalTime,
          type: 'String',
        },
        arrivalTimezone: {
          value: dataWorkflow.arrivalTimezone,
          type: 'String',
        },
        boardingTime: {
          value: dataWorkflow.boardingTime,
          type: 'String',
        },
        seatNumber: {
          value: dataWorkflow.seatNumber,
          type: 'String',
        },
        reservationTime: {
          value: dataWorkflow.reservationTime,
          type: 'String',
        },
        timezone: {
          value: dataWorkflow.timezone,
          type: 'String',
        },
      },
      businessKey: dataWorkflow.bussinessKey,
    };
  }

  generatePaymentTypeForBank(bank: EBankChoice) {
    if (bank === EBankChoice.BCA) {
      return {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'bca',
        },
      };
    }

    if (bank === EBankChoice.BNI) {
      return {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'bni',
        },
      };
    }

    if (bank === EBankChoice.BRI) {
      return {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'bri',
        },
      };
    }

    if (bank === EBankChoice.MANDIRI) {
      return {
        payment_type: 'echannel',
        echannel: {
          bill_info1: 'Payment:',
          bill_info2: 'Online purchase',
        },
      };
    }

    if (bank === EBankChoice.CIMB) {
      return {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'cimb',
        },
      };
    }
  }

  generateResponseCharge(
    bank: EBankChoice,
    originResponse: IChargeResponse,
  ): IChargeTransaction {
    if (
      bank === EBankChoice.BCA ||
      bank === EBankChoice.BNI ||
      bank === EBankChoice.BRI ||
      bank === EBankChoice.CIMB
    ) {
      return {
        bank,
        virtualAccountNumber: originResponse.va_numbers[0].va_number,
      };
    }

    if (bank === EBankChoice.MANDIRI) {
      return {
        bank,
        billerCode: originResponse.biller_code,
        billerKey: originResponse.bill_key,
      };
    }
  }

  calculateDuration(
    redeemTimeUTC: Date,
    flightTime: string,
    timezoneCode: ETimezoneCode,
  ) {
    const flightTimeUTC = new Date(flightTime);

    let adjustedWithTimezone: Date;
    if (timezoneCode === ETimezoneCode.WIB) {
      adjustedWithTimezone = new Date(flightTimeUTC);
    } else if (timezoneCode === ETimezoneCode.WITA) {
      adjustedWithTimezone = new Date(
        flightTimeUTC.getTime() - 1 * 60 * 60 * 1000,
      );
    } else {
      adjustedWithTimezone = new Date(
        flightTimeUTC.getTime() - 2 * 60 * 60 * 1000,
      );
    }

    const msInMinutes = 1000 * 60;
    return Math.round(
      (adjustedWithTimezone.getTime() - redeemTimeUTC.getTime()) / msInMinutes,
    );
  }
}
