export interface ICommitPayment {
  reservationId: string;
  commitPaymentTime: string;
}
