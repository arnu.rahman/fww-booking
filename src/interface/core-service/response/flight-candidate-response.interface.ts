import { IAirport } from '../airport.interface';
import { IAirplane } from '../airplane.interface';
import { IPrice } from '../price.interface';

export interface IFlightCandidateResponse {
  id: string;
  code: string;
  airplane: IAirplane;
  origin: IAirport;
  destination: IAirport;
  departureTime: string;
  arrivalTime: string;
  durationInMinutes: number;
  prices: IPrice[];
}
