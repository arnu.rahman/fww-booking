import { ETimezone } from 'src/helpers/enum/timezone.enum';

export interface IAirport {
  id: string;
  icao: string;
  iata: string;
  name: string;
  city: string;
  timezone: ETimezone;
}
