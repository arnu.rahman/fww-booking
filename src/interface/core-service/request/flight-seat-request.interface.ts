export interface IFlightSeatRequest {
  flight: string;
  seat: string;
}
