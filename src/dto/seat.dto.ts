import {
  IsEnum,
  IsNotEmpty,
  IsString,
  IsOptional,
  IsDateString,
} from 'class-validator';
import { ESeatClass } from 'src/helpers/enum/seat-class.enum';
import { ETimezone } from 'src/helpers/enum/timezone.enum';

export class SeatDto {
  @IsString()
  @IsNotEmpty()
  flightId: string;

  @IsEnum(ESeatClass)
  @IsOptional()
  seatClass: ESeatClass;

  @IsDateString()
  @IsNotEmpty()
  flightDate: string;

  @IsEnum(ETimezone)
  @IsNotEmpty()
  timezone: ETimezone;
}
