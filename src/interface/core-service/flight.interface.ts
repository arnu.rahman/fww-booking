import { IAirplane } from './airplane.interface';
import { IAirport } from './airport.interface';
import { IBaggage } from './baggage.interface';
import { IPrice } from './price.interface';

export interface IFlight {
  id: string;
  code: string;
  airplane: IAirplane;
  origin?: IAirport | string;
  destination?: IAirport | string;
  flightDate?: string;
  departureTime?: string;
  arrivalTime?: string;
  durationInMinutes?: number;
  prices?: IPrice[]
  baggage?: IBaggage[];
}
