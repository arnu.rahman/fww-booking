import { IAirplane } from '../airplane.interface';
import { IBaggage } from '../baggage.interface';

export interface IFlightBaggageResponse {
  id: string;
  code: string;
  origin: string;
  destination: string;
  airplane: IAirplane;
  departureTime: string;
  arrivalTime: string;
  durationInMinutes: number;
  baggage?: IBaggage[];
}
