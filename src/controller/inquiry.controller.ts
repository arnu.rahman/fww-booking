import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  HttpCode,
  Logger,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { FilterAirportDto } from 'src/dto/filter-airport.dto';
import { ChoseSeatDto } from 'src/dto/chose-seat.dto';
import { InquiryService } from 'src/services/inquiry.service';
import { CoreService } from 'src/services/core.service';
import { SeatDto } from 'src/dto/seat.dto';
import JwtGuard from 'src/auth/jwt.guard';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Inquiry')
@Controller({ path: 'inquiry', version: '1' })
export class InquiryController {
  private readonly logger = new Logger(InquiryController.name);
  constructor(
    private readonly inquiryService: InquiryService,
    private readonly coreService: CoreService,
  ) {}

  @UseGuards(JwtGuard)
  @Get('search/airport')
  async searchAirport(@Query() airportDto: FilterAirportDto) {
    this.logger.log('[GET] /api/v1/search/airport');
    try {
      const airports = await this.coreService.fetchAirport(airportDto);
      this.logger.log(
        `Return data airports with criteria ${JSON.stringify(airportDto)}`,
      );
      return airports;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(JwtGuard)
  @HttpCode(200)
  @Post('search/flight-candidate')
  async searchFlightCandidate(@Body() flightDto: FlightCandidateDto) {
    this.logger.log(`[POST] /api/v1/search/flight-candidate`);
    try {
      const flightCandidates = await this.inquiryService.getFlightCandidate(
        flightDto,
      );
      this.logger.log(
        `Return data flight candidates ${JSON.stringify(flightDto)}`,
      );
      return flightCandidates;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(JwtGuard)
  @Get('info/flight/:id/baggage')
  async infoFlighBaggage(@Param('id') id: string) {
    this.logger.log(`[GET] /api/v1/info/flight/${id}/baggage`);
    try {
      const baggage = await this.inquiryService.getInfoBaggage(id);
      this.logger.log(`Return data flight ${id} with baggage`);
      return baggage;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(JwtGuard)
  @HttpCode(200)
  @Post('info/flight/available-seat')
  async infoFlighAvailableSeat(@Body() seatDto: SeatDto) {
    this.logger.log(`[POST] /api/v1/info/flight/available-seat`);
    try {
      const seats = await this.inquiryService.getAvailableSeat(seatDto);
      this.logger.log(
        `Return data flight with available seat ${JSON.stringify(seatDto)}`,
      );
      return seats;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(JwtGuard)
  @HttpCode(200)
  @Post('choose-seat')
  async chooseSeat(@Body() seatDto: ChoseSeatDto) {
    this.logger.log(`[POST] /api/v1/choose-seat`);
    try {
      const result = await this.inquiryService.chooseSeat(seatDto);
      this.logger.log('Return data choosen seat');
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(JwtGuard)
  @HttpCode(200)
  @Post('unchoose-seat')
  async unchooseSeat(@Body() seatDto: ChoseSeatDto) {
    this.logger.log(`[POST] /api/v1/unchoose-seat`);
    try {
      const result = await this.inquiryService.unchooseSeat(seatDto);
      this.logger.log('Return data unchoose seat');
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
}
