import { ISeat } from "./seat.interface";

export interface IAirplane {
  id?: string;
  code: string;
  name: string;
  registrationNumber: string;
  maxBusiness: number;
  maxEconomy: number;
  totalSeat: number;
  seats?: ISeat[]
}
