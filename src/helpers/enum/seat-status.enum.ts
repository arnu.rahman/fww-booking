export enum ESeatStatus {
  SELECTED = 'SELECTED',
  AVAILABLE = 'AVAILABLE',
  BOOKED = 'BOOKED',
  PAID = 'PAID',
  UNPAID = 'UNPAID',
}
