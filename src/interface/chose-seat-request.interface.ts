import { ESeatClass } from 'src/helpers/enum/seat-class.enum';
import { ESeatStatus } from 'src/helpers/enum/seat-status.enum';

export interface IChoseSeatRequest {
  passengerIdentity: string;
  flightDate: string;
  flightId: string;
  originAirportId: string;
  destinationAirportId: string;
  airplaneId: string;
  seatId: string;
  seatClass: ESeatClass;
  seatNumber: string;
  price: number;
  status?: ESeatStatus;
}
