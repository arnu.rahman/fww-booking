export enum EventPatternMessage {
  REPORT_TASK_WORKFLOW = 'EPM_ReportTaskWorkflow',
  SEND_NORMAL_MAIL = 'EPM_SendNormalMail',
  SEND_DECLINE_MAIL = 'EPM_SendDeclineMail',
  UPDATE_SEAT_STATUS = 'EPM_UpdateSeatStatus',
  UPDATE_RESERVATION = 'EPM_UpdateReservation',
  CHARGE_PAYMENT = 'EPM_ChargePayment',
  UPDATE_PAYMENT = 'EPM_UpdatePayment',
}
