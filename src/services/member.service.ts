import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, lastValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import {
  IAxiosBaseError,
  IAxiosError,
} from 'src/interface/axios-error.interface';

@Injectable()
export class MemberService {
  private readonly logger = new Logger(MemberService.name);

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
  ) { }

  private readonly baseUrl = `http://${this.configService.get<string>(
    'MEMBER_HOST',
  )}:${this.configService.get<string>('MEMBER_PORT')}/api`;

  async fetchMember(memberId: string) {
    const url = `${this.baseUrl}/v1/is-valid-member/${memberId}`;

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.get(url, { auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch data member from member engine');
    return data;
  }
}
