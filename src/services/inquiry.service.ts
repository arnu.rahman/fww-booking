import {
  BadRequestException,
  Injectable,
  Logger,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ChoseSeatDto } from 'src/dto/chose-seat.dto';
import { SeatStatusRepository } from 'src/repository/seat-status.repository';
import { CoreService } from './core.service';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { SeatDto } from 'src/dto/seat.dto';
import { IFlightCandidateRequest } from 'src/interface/core-service/request/flight-candidate-request.interface';
import { HelperService } from './helper.service';
import { IFlight } from 'src/interface/core-service/flight.interface';
import { ISeatValidationResponse } from 'src/interface/core-service/response/seat-validation-response.interface';
import { IChoseSeatResult } from 'src/interface/chose-seat-result.interface';
import { IChoseSeatRequest } from 'src/interface/chose-seat-request.interface';
import * as moment from 'moment';

@Injectable()
export class InquiryService {
  private readonly logger = new Logger(InquiryService.name);

  constructor(
    private readonly coreService: CoreService,
    private readonly helperService: HelperService,
    private readonly seatStatusRepo: SeatStatusRepository,
  ) { }

  async getFlightCandidate(flightDto: FlightCandidateDto): Promise<IFlight[]> {
    const todayString =
      this.helperService.formatingTimeWithTimezone(
        flightDto.timezone,
      );

    //validate if flight date before today
    if (moment(flightDto.flightDate).isBefore(todayString)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${todayString}`,
      );
    }

    const flightCandidateReq: IFlightCandidateRequest = {
      originAirportId: flightDto.originAirportId,
      destinationAirportId: flightDto.destinationAirportId,
    };

    const result = await Promise.all([
      this.coreService.fetchFlightCandidate(flightCandidateReq),
      this.seatStatusRepo.getOccupationSummary(flightDto),
    ]);

    const flightCandidates = result[0];
    const occupations = result[1];

    const reformattedFlightCandidates =
      this.helperService.formattingFlightCandidate(
        flightCandidates,
        occupations,
        flightDto.flightDate,
      );

    this.logger.log('Process data flight candidates');
    return reformattedFlightCandidates;
  }

  async getInfoBaggage(id: string): Promise<IFlight> {
    const flightWithBaggages = await this.coreService.fetchFlightBaggage(id);
    const reformattedFlightWithBaggage =
      this.helperService.formattingInfoBaggage(flightWithBaggages);

    this.logger.log('Process data flight with info baggage');
    return reformattedFlightWithBaggage;
  }

  async getAvailableSeat(seatDto: SeatDto) {
    const todayString =
      this.helperService.formatingTimeWithTimezone(
        seatDto.timezone,
      );

    //validate if flight date before today
    if (moment(seatDto.flightDate).isBefore(todayString)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${todayString}`,
      );
    }

    const result = await Promise.all([
      this.coreService.fetchFlightSeat(seatDto.flightId, seatDto.seatClass),
      this.seatStatusRepo.getOccupied(seatDto),
    ]);
    const flightSeats = result[0];
    const occupiedSeats = result[1];
    const occupiedSeatIds = occupiedSeats.map((occ) => occ.seatId);
    const availableSeats = this.helperService.formattingAvailableSeat(
      flightSeats,
      occupiedSeatIds,
    );

    this.logger.log('Reformat data available seat');
    return availableSeats;
  }

  async chooseSeat(seatDto: ChoseSeatDto): Promise<IChoseSeatResult> {
    const { passengerIdentity, flightDate, timezone, flightId, seatId } = seatDto;
    const todayString = this.helperService.formatingTimeWithTimezone(timezone);

    //validate if flight date before today
    if (moment(flightDate).isBefore(todayString)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${todayString}`,
      );
    }

    const anotherSeatStillLocked = await this.seatStatusRepo.checkAnotherSelected(
      passengerIdentity,
      flightDate,
      seatId,
    );

    if (anotherSeatStillLocked) {
      throw new UnprocessableEntityException(
        `You still have another selected seat, need release first: ${anotherSeatStillLocked.flightDate} - ${anotherSeatStillLocked.flightId} - ${anotherSeatStillLocked.seatId}`,
      );
    }

    const validationResponse: ISeatValidationResponse =
      await this.coreService.validateSeat(flightId, seatId);

    const { isValid, ...seatValid } = validationResponse;

    if (!isValid) {
      throw new UnprocessableEntityException('Invalid input seat info');
    }

    const choseSeatRequest: IChoseSeatRequest = {
      passengerIdentity,
      flightDate,
      flightId: seatValid.flight,
      seatId: seatValid.seat,
      airplaneId: seatValid.airplane,
      originAirportId: seatValid.origin,
      destinationAirportId: seatValid.destination,
      ...seatValid
    };

    const seatChosen = await this.seatStatusRepo.setChooseSeat(
      choseSeatRequest,
    );
    this.logger.log('Process choose seat');

    return {
      passenger: seatChosen.passengerIdentity,
      flightDate: seatChosen.flightDate,
      flightId: seatChosen.flightId,
      seatId: seatChosen.seatId,
      status: seatChosen.status,
    } as IChoseSeatResult;
  }

  async unchooseSeat(seatDto: ChoseSeatDto): Promise<IChoseSeatResult> {
    const { passengerIdentity, timezone, flightDate, flightId, seatId } = seatDto;
    const todayString = this.helperService.formatingTimeWithTimezone(timezone);

    //validate if flight date before today
    if (moment(flightDate).isBefore(todayString)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${todayString}`,
      );
    }

    const validationResponse = await this.coreService.validateSeat(
      flightId,
      seatId,
    );

    const { isValid, ...seatValid } = validationResponse;

    const choseSeatRequest: IChoseSeatRequest = {
      passengerIdentity,
      flightDate,
      flightId: seatValid.flight,
      seatId: seatValid.seat,
      airplaneId: seatValid.airplane,
      originAirportId: seatValid.origin,
      destinationAirportId: seatValid.destination,
      ...seatValid
    };

    if (!isValid) {
      throw new UnprocessableEntityException('Invalid input seat info');
    }
    console.log(choseSeatRequest)
    const seatUnchoose = await this.seatStatusRepo.setUnchooseSeat(
      choseSeatRequest,
    );
    this.logger.log('Process unchoose seat');
    return {
      passenger: seatUnchoose.passengerIdentity,
      flightDate: seatUnchoose.flightDate,
      flightId: seatUnchoose.flightId,
      seatId: seatUnchoose.seatId,
      status: seatUnchoose.status,
    } as IChoseSeatResult;
  }
}
