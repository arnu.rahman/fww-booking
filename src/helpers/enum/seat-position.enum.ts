export enum ESeatPosition {
  WINDOW = 'Window',
  MIDDLE = 'Middle',
  AISLE = 'Aisle',
}
