import {
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsString,
} from 'class-validator';
import { ETimezone } from 'src/helpers/enum/timezone.enum';

export class FlightCandidateDto {
  @IsString()
  @IsNotEmpty()
  originAirportId: string;

  @IsString()
  @IsNotEmpty()
  destinationAirportId: string;

  @IsDateString()
  @IsNotEmpty()
  flightDate: string;

  @IsEnum(ETimezone)
  @IsNotEmpty()
  timezone: ETimezone;
}
