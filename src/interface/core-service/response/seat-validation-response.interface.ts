import { ESeatClass } from 'src/helpers/enum/seat-class.enum';

export interface ISeatValidationResponse {
  isValid: boolean;
  flight: string;
  airplane: string;
  origin?: string;
  destination?: string;
  seat: string;
  seatClass: ESeatClass;
  seatNumber: string;
  price: number;
}
