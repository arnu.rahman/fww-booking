import {
  Injectable,
  Logger,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { SeatStatus } from 'src/entities/seat-status.entity';
import { ESeatStatus } from 'src/helpers/enum/seat-status.enum';
import { DataSource, In, Not, Repository } from 'typeorm';
import { SeatDto } from 'src/dto/seat.dto';
import { IReservationCheck } from 'src/interface/reservation-check.interface';
import { ISeatStatusUpdate } from 'src/interface/seat-status-update.interface';
import { IChoseSeatRequest } from 'src/interface/chose-seat-request.interface';
import { IOccupiedFlightCandidate } from 'src/interface/occupied-flight-candidate.interface';

@Injectable()
export class SeatStatusRepository extends Repository<SeatStatus> {
  private readonly logger = new Logger(SeatStatusRepository.name);
  constructor(dataSource: DataSource) {
    super(SeatStatus, dataSource.createEntityManager());
  }

  //get ringkatan data fligt dan seat yang telah terisi
  async getOccupationSummary(
    flightDto: FlightCandidateDto,
  ): Promise<IOccupiedFlightCandidate[]> {
    const occupiedQB = this.createQueryBuilder('seat_status')
      .select('seat_status.flightDate', 'flighDate')
      .addSelect('seat_status.flight_id', 'flight')
      .addSelect('seat_status.seatClass', 'seatClass')
      .addSelect('COUNT(*)', 'occupied')
      .where('seat_status.flightDate = :flightDate', {
        flightDate: flightDto.flightDate,
      })
      .andWhere('seat_status.origin_airport_id = :originAirportId', {
        originAirportId: flightDto.originAirportId,
      })
      .andWhere('seat_status.destination_airport_id = :destinationAirportId', {
        destinationAirportId: flightDto.destinationAirportId,
      })
      .andWhere('seat_status.status <> :seatStatus', {
        seatStatus: ESeatStatus.AVAILABLE,
      })
      .groupBy('seat_status.flightDate')
      .addGroupBy('seat_status.flight_id')
      .addGroupBy('seat_status.seatClass');

    const occupiedFlightCandidates = await occupiedQB.getRawMany();

    this.logger.log('Query data occupied flight candidates with criteria');
    return occupiedFlightCandidates as IOccupiedFlightCandidate[];
  }

  //get status seat yg telah terisi
  async getOccupied(seatDto: SeatDto) {
    const { timezone, ...criteria } = seatDto;
    const occupied = await this.find({
      where: {
        ...criteria,
        status: In([
          ESeatStatus.SELECTED,
          ESeatStatus.BOOKED,
          ESeatStatus.PAID,
        ]),
      },
    });
    this.logger.log('Query data flight with seat already occupied');
    return occupied;
  }

  async checkAnotherSelected(
    passengerIdentity: string,
    flightDate: string,
    seatId: string,
  ): Promise<SeatStatus> {
    const seatStillSelected = await this.findOne({
      where: {
        passengerIdentity,
        flightDate,
        seatId: Not(seatId),
        status: ESeatStatus.SELECTED,
      },
    });

    this.logger.log('Check another seat still lock');
    return seatStillSelected;
  }

  async setChooseSeat(
    chooseSeatRequest: IChoseSeatRequest,
  ): Promise<SeatStatus> {
    const seatExist = await this.findOne({
      where: {
        flightDate: chooseSeatRequest.flightDate,
        flightId: chooseSeatRequest.flightId,
        seatId: chooseSeatRequest.seatId,
      },
    });

    const newChoseSeatRequest: IChoseSeatRequest = {
      ...chooseSeatRequest,
      status: ESeatStatus.SELECTED,
    };

    if (!seatExist) {
      const seatChoosen = await this.save(newChoseSeatRequest);
      this.logger.log(
        `Seat choosen: ${chooseSeatRequest.flightDate} - ${chooseSeatRequest.flightId} - ${chooseSeatRequest.seatId}`,
      );
      return seatChoosen;
    } else if (seatExist.status === ESeatStatus.AVAILABLE || seatExist.status === ESeatStatus.UNPAID) {
      seatExist.passengerIdentity = chooseSeatRequest.passengerIdentity;
      seatExist.status = ESeatStatus.SELECTED;
      const seatChoosen = await this.save(seatExist);
      this.logger.log(
        `Seat choosen: ${chooseSeatRequest.flightDate} - ${chooseSeatRequest.flightId} - ${chooseSeatRequest.seatId}`,
      );
      return seatChoosen;
    } else {
      throw new UnprocessableEntityException(
        `Failed to choose seat : ${chooseSeatRequest.flightDate} - ${chooseSeatRequest.flightId} - ${chooseSeatRequest.seatId}`,
      );
    }
  }

  //set unchoose seat
  async setUnchooseSeat(
    chooseSeat: IChoseSeatRequest,
  ): Promise<SeatStatus> {
    const seatExist = await this.findOne({
      where: {
        flightDate: chooseSeat.flightDate,
        flightId: chooseSeat.flightId,
        seatId: chooseSeat.seatId,
        passengerIdentity: chooseSeat.passengerIdentity,
        airplaneId: chooseSeat.airplaneId,
        originAirportId: chooseSeat.originAirportId,
        destinationAirportId: chooseSeat.destinationAirportId
      },
    });

    //kalau kosong ga bisa diupdate datanya
    if (!seatExist) {
      throw new NotFoundException(
        `Failed to unchoose seat : ${chooseSeat.flightDate} - ${chooseSeat.flightId} - ${chooseSeat.seatId}`,
      );
    }

    //update status jadi available jika ga jadi dipilih
    if (seatExist.status === ESeatStatus.SELECTED) {
      seatExist.status = ESeatStatus.AVAILABLE;
      const seatUnchoose = await this.save(seatExist);
      this.logger.log(
        `Set seat unchoose: ${chooseSeat.flightDate} - ${chooseSeat.flightId} - ${chooseSeat.seatId}`,
      );
      return seatUnchoose;
    } else {
      //kalau statusnya bukan selected ga bisa diupdate datanya
      throw new UnprocessableEntityException(
        `Failed to unchoose seat : ${chooseSeat.flightDate} - ${chooseSeat.flightId} - ${chooseSeat.seatId}`,
      );
    }
  }

  //get seat stattus by passenger identity and flight date
  async findOneByPassengerAndFlightDate(
    reservationCheck: IReservationCheck,
  ): Promise<SeatStatus> {
    const seatSelectedByPassenger = await this.findOne({
      where: reservationCheck,
    });

    this.logger.log('Query get data seat status by passenger_id and flight date');
    return seatSelectedByPassenger;
  }

  //update status seat
  async updateStatus(seatStatusData: ISeatStatusUpdate): Promise<SeatStatus> {
    const { seatStatus, ...search } = seatStatusData;
    const seatExist = await this.findOne({
      where: search,
    });

    seatExist.status = seatStatus;
    await this.save(seatExist);
    this.logger.log(
      `Set seat status: ${seatExist.flightDate} - ${seatExist.flightId} - ${seatExist.seatId} - ${seatExist.status}`,
    );
    return seatExist;
  }
}
