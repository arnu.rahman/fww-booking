import { ESeatClass } from 'src/helpers/enum/seat-class.enum';
import { ESeatStatus } from 'src/helpers/enum/seat-status.enum';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Unique,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Unique(['flightDate', 'flightId', 'seatId'])
@Entity('seat_status')
export class SeatStatus {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 16, name: 'passenger_identity' })
  passengerIdentity: string;

  @Column({ name: 'flight_date', type: 'date' })
  flightDate: string;

  @Column({ name: 'flight_id', type: 'uuid' })
  flightId: string;

  @Column({ name: 'origin_airport_id', type: 'uuid' })
  originAirportId: string;

  @Column({ name: 'destination_airport_id', type: 'uuid' })
  destinationAirportId: string;

  @Column({ name: 'airplane_id', type: 'uuid' })
  airplaneId: string;

  @Column({ name: 'seat_id', type: 'uuid' })
  seatId: string;

  @Column({ name: 'seat_class', length: 10 })
  seatClass: ESeatClass;

  @Column({ name: 'seat_number', length: 10 })
  seatNumber: string;

  @Column({ type: 'double' })
  price: number;

  @Column({ length: 10 })
  status: ESeatStatus;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
