import { PickType } from '@nestjs/swagger';
import { PartnerDto } from './partner.dto';

export class RegisterRequestDto extends PickType(PartnerDto, [
  'name',
  'username',
  'password',
  'email',
  'phone',
]) {}
