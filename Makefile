SERVICE_NAME := fww-booking
GIT_COMMIT_ID := $(shell git log --format="%H" -n 1)
IMAGE_ID := $(shell docker images --filter=reference=arnurahman/fww-booking --format "{{.ID}}")
CONTAINER_ID := $(shell docker ps -aqf "name=fww-booking")

rebase:
			git fetch && git pull --rebase

prune:
			docker stop $(CONTAINER_ID)
			docker rm $(CONTAINER_ID)
			docker rmi -f $(IMAGE_ID)

build:
			docker build -t arnurahman/${SERVICE_NAME}:latest .

push:
			docker push arnurahman/${SERVICE_NAME}:latest
			docker push arnurahman/${SERVICE_NAME}:$(GIT_COMMIT_ID)

run:
			docker run --env-file /mnt/c/xampp/htdocs/fww-airlines/fww-booking/service-booking.env --name ${SERVICE_NAME} --link mariadb --link rabbitmq --link camunda --link fww-core-engine --link fww-member -p $(cport):$(cport) -d arnurahman/${SERVICE_NAME}:latest

logs:
			docker logs -f ${SERVICE_NAME}
