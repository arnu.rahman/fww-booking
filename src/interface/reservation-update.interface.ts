import { EReservationStatus } from 'src/helpers/enum/reservation-status.enum';

export interface IReservationUpdateRequest {
  id: string;
  status: EReservationStatus;
  journeyTime: string;
  bookingCode?: string;
  reservationCode?: string;
}

export interface IReservationUpdateData {
  currentStatus: EReservationStatus;
  bookingCode?: string;
  reservationCode?: string;
}
