export interface IRegisterPartner {
  cognitoId: string;
  username: string;
  name: string;
  email: string;
  phone: string;
}
