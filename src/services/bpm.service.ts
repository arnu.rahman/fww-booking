import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, lastValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import {
  IAxiosBaseError,
  IAxiosError,
} from 'src/interface/axios-error.interface';
import { IStartProcess } from 'src/interface/bpm-service/start-process.interface';
import { HelperService } from './helper.service';

@Injectable()
export class BPMService {
  private readonly logger = new Logger(BPMService.name);

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
    private readonly helperService: HelperService,
  ) {}

  private readonly baseUrl = `http://${this.configService.get<string>(
    'BPM_HOST',
  )}:${this.configService.get<string>('BPM_PORT')}/engine-rest`;

  async startProcess(dataWorkflow: IStartProcess) {
    const url = `${
      this.baseUrl
    }/process-definition/key/${this.configService.get<string>(
      'BPM_KEY',
    )}/start`;

    const headersRequest = {
      Accept: 'application/json',
    };

    const config = {
      headers: headersRequest,
      timeout: 5000,
    };

    const reformattedDataWorkflow =
      this.helperService.formattingStartProcessWorkflow(dataWorkflow);

    const { data } = await lastValueFrom(
      this.httpService.post(url, reformattedDataWorkflow, config).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Start process workflow');
    return data;
  }

  async viewTask(id: string) {
    const url = `${this.baseUrl}/task?processInstanceId=${id}`;

    const headersRequest = {
      Accept: 'application/json',
    };

    const config = {
      headers: headersRequest,
      timeout: 5000,
    };

    const { data } = await lastValueFrom(
      this.httpService.get(url, config).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Get current tasks camunda');
    return data;
  }

  async completeTask(id: string, dataWF = {}) {
    const url = `${this.baseUrl}/task/${id}/complete`;

    const headersRequest = {
      Accept: 'application/json',
    };

    const config = {
      headers: headersRequest,
      timeout: 5000,
    };

    const result = await lastValueFrom(
      this.httpService.post(url, dataWF, config).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Complete a task camunda');
    return result;
  }

  async getVariables(id: string) {
    const url = `${this.baseUrl}/task/${id}/variables`;

    const headersRequest = {
      Accept: 'application/json',
    };

    const config = {
      headers: headersRequest,
      timeout: 5000,
    };

    const { data } = await lastValueFrom(
      this.httpService.get(url, config).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Get variables task from camunda');
    return data;
  }
}
