import { ESeatClass } from 'src/helpers/enum/seat-class.enum';

export interface IOccupiedFlightCandidate {
  flightDate: string;
  flight: string;
  seatClass: ESeatClass;
  occupied: number;
}
