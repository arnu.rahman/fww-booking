import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, lastValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import { FilterAirportDto } from 'src/dto/filter-airport.dto';
import {
  IAxiosBaseError,
  IAxiosError,
} from 'src/interface/axios-error.interface';
import { IFlightCandidateResponse } from 'src/interface/core-service/response/flight-candidate-response.interface';
import { IReservationNew } from 'src/interface/reservation-new.interface';
import { IReservationResult } from 'src/interface/reservation-result.interface';
import { ReservationCodeDto } from 'src/dto/reservation-code.dto';
import { IFlightCandidateRequest } from 'src/interface/core-service/request/flight-candidate-request.interface';
import { IAirport } from 'src/interface/core-service/airport.interface';
import { IFlightBaggageResponse } from 'src/interface/core-service/response/flight-baggage-response.interface';
import { ESeatClass } from 'src/helpers/enum/seat-class.enum';
import { IFlightSeatResponse } from 'src/interface/core-service/response/flight-seat-response.interface';
import { IFlightSeatRequest } from 'src/interface/core-service/request/flight-seat-request.interface';
import { ISeatValidationResponse } from 'src/interface/core-service/response/seat-validation-response.interface';

@Injectable()
export class CoreService {
  private readonly logger = new Logger(CoreService.name);

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
  ) {}

  private readonly baseUrl = `http://${this.configService.get<string>(
    'CORE_HOST',
  )}:${this.configService.get<string>('CORE_PORT')}/api`;

  async fetchAirport(airportDto: FilterAirportDto): Promise<IAirport[]> {
    const url = `${this.baseUrl}/v1/inquiry/airport`;

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    const params = {};
    if (airportDto.city) {
      Object.assign(params, { city: airportDto.city });
    }

    if (airportDto.name) {
      Object.assign(params, { name: airportDto.name });
    }

    if (airportDto.icao) {
      Object.assign(params, { code: airportDto.icao });
    }

    if (airportDto.iata) {
      Object.assign(params, { code: airportDto.iata });
    }

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.get(url, { auth, params }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch data airports from core engine');
    return data as IAirport[];
  }

  async fetchFlightCandidate(flightCandidate: IFlightCandidateRequest) {
    const url = `${this.baseUrl}/v1/inquiry/flight-candidate`;

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.post(url, flightCandidate, { auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch data flight candidates from core engine');
    return data as IFlightCandidateResponse[];
  }

  async fetchFlightBaggage(flightID: string): Promise<IFlightBaggageResponse> {
    const url = `${this.baseUrl}/v1/inquiry/flight/${flightID}/baggage`;

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.get(url, { auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch data flight with baggage from core engine');
    return data as IFlightBaggageResponse;
  }

  async fetchFlightAirport(flightID: string) {
    const url = `${this.baseUrl}/v1/inquiry/flight/${flightID}/airport`;

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.get(url, { auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch data flight with airport from core engine');
    return data;
  }

  async fetchFlightSeat(
    id: string,
    seatClass: ESeatClass,
  ): Promise<IFlightSeatResponse> {
    const url = `${this.baseUrl}/v1/inquiry/flight/${id}/seat`;

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    let params = {};
    if (seatClass) {
      params = { seatClass };
    }

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.get(url, { auth, params }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch data flight with seat from core engine');
    return data as IFlightSeatResponse;
  }

  async validateSeat(
    flight: string,
    seat: string,
  ): Promise<ISeatValidationResponse> {
    const url = `${this.baseUrl}/v1/inquiry/seat-validate`;

    const flightSeat: IFlightSeatRequest = {
      flight,
      seat,
    };

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.post(url, flightSeat, { auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Validate flight seat from core engine');
    return data as ISeatValidationResponse;
  }

  async submitReservation(reservation: IReservationNew) {
    const url = `${this.baseUrl}/v1/reservation`;

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.post(url, reservation, { auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Submit new reservation to core engine');
    return data;
  }

  async fetchReservation(reservationID: string) {
    const url = `${this.baseUrl}/v1/reservation/${reservationID}`;

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.get(url, { auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch data reservation from core engine');
    return data as IReservationResult;
  }

  async fetchReservationByCode(reservationDto: ReservationCodeDto) {
    const { identityNumber, reservationCode } = reservationDto;
    const url = `${this.baseUrl}/v1/reservation/by-code/${identityNumber}/${reservationCode}`;

    const auth = {
      username: this.configService.get<string>('BASIC_USER'),
      password: this.configService.get<string>('BASIC_PASS_UNHASH'),
    };

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.get(url, { auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch data reservation from core engine');
    return data as IReservationResult;
  }
}
