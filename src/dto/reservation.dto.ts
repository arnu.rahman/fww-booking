import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUUID,
  Length,
} from 'class-validator';
import { EIdentityCat } from 'src/helpers/enum/identity-category.enum';
import { ETimezone } from 'src/helpers/enum/timezone.enum';

export class ReservationDto {
  @IsEnum(EIdentityCat)
  @IsNotEmpty()
  identityCategory: EIdentityCat;

  @IsString()
  @Length(16)
  @IsNotEmpty()
  identityNumber: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsDateString()
  @IsNotEmpty()
  birthDate: string;

  @IsPhoneNumber()
  @IsNotEmpty()
  phone: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsOptional()
  @IsUUID()
  memberId: string;

  @IsISO8601({ strict: true })
  @IsDateString()
  @IsNotEmpty()
  flightDate: string;

  @IsUUID()
  @IsNotEmpty()
  flightId: string;

  @IsUUID()
  @IsNotEmpty()
  seatId: string;

  @IsEnum(ETimezone)
  @IsNotEmpty()
  timezone: ETimezone;
}
