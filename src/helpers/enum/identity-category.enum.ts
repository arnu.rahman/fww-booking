export enum EIdentityCat {
  KTP = 'KTP',
  KTA = 'KTA',
  PASSPORT = 'Passport',
  SIM = 'SIM',
  OTHER = 'Other'
}
