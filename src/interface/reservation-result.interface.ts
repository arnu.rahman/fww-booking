import { EReservationStatus } from 'src/helpers/enum/reservation-status.enum';
import { IPassenger } from './core-service/passenger.interface';
import { IFlight } from './core-service/flight.interface';
import { IPaymentMaster } from './payment.interface';

export interface IReservationResult {
  id: string;
  bookingCode: string;
  reservationCode: string;
  partnerId: string;
  memberId: string;
  passenger: IPassenger;
  flight: IFlight;
  flightDate: string;
  payments: IPaymentMaster
  priceActual: number;
  currentStatus: EReservationStatus;
}
