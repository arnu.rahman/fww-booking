import { DataSource, DataSourceOptions } from "typeorm";
import * as dotenv from 'dotenv'
import { SeatStatus } from "../entities/seat-status.entity";
import { Partner } from "../entities/partner.entity";
import { Workflow } from "../entities/workflow.entity";
import { WorkflowDetail } from "../entities/workflow-detail.entity";
dotenv.config();

export const dataSourceOptions: DataSourceOptions = {
    type: 'mariadb',
    host: process.env.MYSQL_HOST,
    port: parseInt(process.env.MYSQL_PORT),
    username: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database:  process.env.MYSQL_DATABASE,
    entities: [SeatStatus, Partner, Workflow, WorkflowDetail],
    synchronize: false,
    logging: false,
    dateStrings: true
}

const dataSource = new DataSource(dataSourceOptions);
dataSource.initialize();
export default dataSource;
