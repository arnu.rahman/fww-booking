import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  HttpCode,
  InternalServerErrorException,
  Logger,
  Param,
  ParseUUIDPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import { IWorkflowDetail } from 'src/interface/workflow.interface';
import { EventPattern, Payload } from '@nestjs/microservices';
import { WorkflowService } from 'src/services/workflow.service';
import { ChargePaymentDto } from 'src/dto/charge-payment.dto';
import { AuthGuard } from '@nestjs/passport';
import { ISeatStatusUpdate } from 'src/interface/seat-status-update.interface';
import JwtGuard from 'src/auth/jwt.guard';
import { EventPatternMessage } from 'src/helpers/enum/pattern-message.enum';

@Controller({ version: '1' })
export class WorkflowController {
  private readonly logger = new Logger(WorkflowController.name);
  constructor(private readonly workflowService: WorkflowService) {}

  @UseGuards(AuthGuard('basic'))
  @Get('check-payment/:paymentId')
  async checkPayment(@Param('paymentId', ParseUUIDPipe) paymentId: string) {
    try {
      this.logger.log(`[GET] /api/v1/check-payment/${paymentId}`);
      const result = await this.workflowService.checkPayment(paymentId);
      this.logger.log('Return status midtrans');
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(JwtGuard)
  @Post('commit-payment')
  async commitPayment(@Body('reservationId') reservationId: string) {
    try {
      this.logger.log(`[POST] /api/v1/commit-payment`);
      const result = await this.workflowService.commitPayment(reservationId);
      this.logger.log('Process workflow commit payment done');
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(JwtGuard)
  @HttpCode(200)
  @Post('charge-transaction')
  async chargeTransaction(@Body() chargeDto: ChargePaymentDto) {
    try {
      this.logger.log(`[POST] /api/v1/charge-transaction`);
      const result = await this.workflowService.chargeTransaction(chargeDto);
      this.logger.log('Process workflow commit payment done');
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(JwtGuard)
  @HttpCode(200)
  @Post('confirm-payment')
  async confirmPayment(@Body('reservationId') reservationId: string) {
    try {
      this.logger.log(`[POST] /api/v1/confirm-payment`);
      const result = await this.workflowService.confirmPayment(reservationId);
      this.logger.log('Process workflow confirm payment done');
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }


  @EventPattern(EventPatternMessage.REPORT_TASK_WORKFLOW)
  async handleReportTaskWorkflow(@Payload() data: any) {
    try {
      const dataWorkflow = data as IWorkflowDetail;
      await this.workflowService.insertWorkflowDetail(dataWorkflow);
      this.logger.log('Process data workflow detail done');
    } catch (error) {
      throw new InternalServerErrorException(
        'Error in handleReportTaskWorkflow',
      );
    }
  }

  @EventPattern(EventPatternMessage.UPDATE_SEAT_STATUS)
  async handleUpdateSeatStatus(@Payload() data: any) {
    try {
      const seatStatusUpdate = data as ISeatStatusUpdate;
      await this.workflowService.updateSeatStatus(seatStatusUpdate);
      this.logger.log('Process reset seat status done');
    } catch (error) {
      throw new InternalServerErrorException('Error in handleUpdateSeatStatus');
    }
  }
}
