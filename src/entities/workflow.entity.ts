import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { WorkflowDetail } from './workflow-detail.entity';

@Entity()
export class Workflow {
  @PrimaryColumn({ name: 'process_instance_id', type: 'uuid' })
  processInstanceId: string;

  @Column({ name: 'reservation_id', type: 'uuid' })
  reservationId: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;

  @OneToMany(() => WorkflowDetail, (workflowDetail) => workflowDetail.workflow)
  details?: WorkflowDetail[];
}
