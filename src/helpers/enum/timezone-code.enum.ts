export enum ETimezoneCode {
  WIB = 'WIB',
  WITA = 'WITA',
  WIT = 'WIT',
}
