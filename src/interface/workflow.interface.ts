export interface IWorkflowMaster {
  processInstanceId: string;
  reservationId?: string;
  details?: IWorkflowDetail[];
}

export interface IWorkflowDetail {
  workflow?: IWorkflowMaster;
  taskId: string;
  activityName: string;
}
